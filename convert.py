f1 = open("CommunesFrance.csv","r")
f2 = open("CommunesFrance_10000coord.tgoGraph","r")
f3 = open("CF_co_pop_10000.tgoGraph","w")

lines1 = []
lines2 = []
lines3 = []

for x in f1 :
    words = x.split(';')
    finalwords = []
    finalwords.append(words[0])
    finalwords.append(words[2])
    lines1.append(finalwords)
for x in f2 :
    words = x.split(' ')
    if len(words) == 4 :
        lines2.append(words)

f3.write("Name: CF_co_pop_10000.tgoGraph\nOriented: false\nParameters [nbVertices nbValuesByVertex nbEdges nbValuesByEdge]:\n9496 0 55038 1\nVertices [id name values[] pop]:\n")

for i in range(2,len(lines2)) :
    f3.write(lines2[i][0] + " " + lines2[i][1] + " " + lines2[i][2] + " " + lines2[i][3][0:len(lines2[i][3])-1] + " ")
    j = 1
    while j < len(lines1) and lines1[j][0] != lines2[i][1] :
        j = j + 1
    if(j < len(lines1)) :
        pop = lines1[j][1]
        f3.write(pop + "\n")

print("Done : you only have to add edges")
f3.close()
f2.close()
f1.close()
