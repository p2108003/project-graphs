package graphs;

/**
 * Classe Chrono
 * 
 * Classe uniquement utilis�e pour calculer les temps d'ex�cution des algorithmes
 *
 */

public class Chrono {
	
	// Attributs
	
	private long tempsDepart;
	private long tempsFin;
	private long temps;

	/**
	 * Constructeur
	 */
	
	public Chrono() {
		tempsDepart = 0;
		tempsFin = 0;
		temps = 0;
	}
	
	/**
	 * Lancer le chrono
	 */
	
	public void start() {
		tempsDepart=System.currentTimeMillis();
		tempsFin=0;
	}

	/**
	 * Stopper le chrono
	 */
	
	public void stop() {
		if(tempsDepart==0) {return;}
		tempsFin=System.currentTimeMillis();
		temps=(tempsFin-tempsDepart);
		tempsDepart=0;
		tempsFin=0;
	}        

	/**
	 * Retourne la dur�e en secondes
	 * @return tempsSec
	 */
	
	public long getDureeSec() {
		return temps/1000;
	}

	/**
	 * Retourne la dur�e en milisecondes
	 * @return tempsMs
	 */
	
	public long getDureeMs() {
		return temps;
	}

}