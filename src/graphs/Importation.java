package graphs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

import graphs.composites.Edge;
import graphs.composites.Graph;
import graphs.composites.Vertex;

/**
 * Classe Importation
 * 
 * Classe permettant d'importer un Graphe � partir d'un fichier tgoGraph
 *
 */

public class Importation {
	
	// Attributs
	
	private String name;
	private boolean oriented = false;
	private int parametres[];
	private ArrayList<Vertex> vertices;
	private ArrayList<LinkedList<Edge>> listAdjacence;
	
	private int deg[];
	
	/**
	 * Constructeur
	 * @param fileName
	 */
	
	public Importation(String nomF) {
		
		parametres = new int[4];
		vertices = new ArrayList<Vertex>();
		listAdjacence = new ArrayList<LinkedList<Edge>>();
		try {
			Scanner scan = new Scanner(new File(nomF));
			while (scan.hasNextLine()) {
				String[] mots = scan.nextLine().split(" ");
				
				// Nom du graphe
				
				if(Arrays.asList(mots).contains("Name:")) {
					name = mots[mots.length - 1];
				}
				
				// Orient� ?
				
				if(Arrays.asList(mots).contains("Oriented:")) {
					oriented = Boolean.parseBoolean(mots[mots.length - 1]);
				}
				
				// Param�tres du graphe
				
				if(Arrays.asList(mots).contains("Parameters") || Arrays.asList(mots).contains("Parameters:")) {
					parametres[0] = scan.nextInt(); parametres[1] = scan.nextInt();
					parametres[2] = scan.nextInt(); parametres[3] = scan.nextInt();
				}
				
				// Sommets du graphe
				
				if(Arrays.asList(mots).contains("Vertices") || Arrays.asList(mots).contains("Vertices:")) {
					mots = scan.nextLine().split(" ");
					while(!Arrays.asList(mots).contains("Edges") && !Arrays.asList(mots).contains("Edges:")) {
						int id = Integer.parseInt(mots[0]);
						String name = mots[1];
						double values[] = new double[parametres[1]];
						for(int i = 0; i<parametres[1]; i++) {
							values[i] = Double.parseDouble(mots[i + 2]);
						}
						Vertex ver = null;
						if(mots.length > 4) {
							int pop = Integer.parseInt(mots[parametres[1] + 2]);
							ver = new Vertex(id,name,values,pop);
						} else {
							ver = new Vertex(id,name,values);
						}
						
						vertices.add(ver);
						mots = scan.nextLine().split(" ");
					}
				}
				
				// Liste adjacence du graph

				if(Arrays.asList(mots).contains("Edges") || Arrays.asList(mots).contains("Edges:")) {
					
					ArrayList<Edge> edges = new ArrayList<Edge>();
					
					
					while(scan.hasNextLine()) {
						mots = scan.nextLine().split(" ");
						int idInitV = Integer.parseInt(mots[0]);
						int idFinV = Integer.parseInt(mots[1]);
						double values[] = new double[parametres[3]];
						for(int i = 0; i<parametres[3]; i++) {
							values[i] = Double.parseDouble(mots[i + 2]);
						}
						Edge e = new Edge(idInitV, idFinV, values);
						edges.add(e);
					}
										
					for(Vertex ver : vertices) {
						LinkedList<Edge> el = new LinkedList<Edge>();
						for(Edge e : edges) {
							if(e.getIdInit() == ver.getId()) {
								el.add(e);
							}
						}
						listAdjacence.add(el);
					}
					
					if(oriented == false) {
						for(LinkedList<Edge> el : listAdjacence) {
							int idV = listAdjacence.indexOf(el);
							for(int i = edges.size()-1; i>=0; i--) {
								if(edges.get(i).getIdFin() == idV) {
									Edge tmp_e = new Edge(edges.get(i).getIdFin(), edges.get(i).getIdInit(), edges.get(i).getValues());
									el.addFirst(tmp_e);
								}
							}
						}
					}
				}
			}
			
			// Tableau de degr�s
			
			deg = new int[parametres[0]];
			
			for(Vertex v : vertices) {
				for(LinkedList<Edge> el : listAdjacence) {
					deg[vertices.indexOf(v)] = el.size();
				}
			}
		
			// Cr�ation du graphe
			
			Graph g = new Graph(name, oriented, parametres, vertices, listAdjacence);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
		
}
