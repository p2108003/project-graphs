package graphs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import graphs.algorithms.AStar;
import graphs.algorithms.Dijkstra;
import graphs.algorithms.VRPAverageDistance;
import graphs.algorithms.VRPGrandTour;
import graphs.composites.Graph;
import graphs.composites.Vertex;

/**
 * Classe Main
 * 
 */

public class Main {
	
	// Liste des graphes en m�moire
	
	public static ArrayList<Graph> graphs = new ArrayList<Graph>();

	public static void main(String[] args) throws IOException {
		
		// Chronom�tre
		
		Chrono ch = new Chrono();
		
		// Importation du graphe dans le fichier
		
		Importation i = new Importation("C:\\Users\\jdegu\\OneDrive\\Documents\\Cours Polytech\\S6\\Graphes\\Projet Files\\CF_co_pop_5000.tgoGraph");
		
		//System.out.println(graphs.get(0).furthestCities());
		
		// Algorithme dijkstra avec affichage du chemin
		System.out.println("-- Algorithme Dijkstra (affichage du chemin) --\n");
		ch.start();
		LinkedList<Integer> path = Dijkstra.path(graphs.get(0), 200, 2710);
		ch.stop();
		for(Integer ind : path) {
			System.out.print(graphs.get(0).getVertices().get(ind).getName()+", ");
		}
		System.out.println();
		System.out.println("Temps d'ex�cution : " + ch.getDureeMs() + " ms\n");
		
		// Algorithme AStar avec affichage du chemin
		
		System.out.println("-- Algorithme A* (affichage du chemin) --\n");
		ch.start();
		path = AStar.path(graphs.get(0), 200,2710);
		ch.stop();
		for(Integer ind : path) {
			System.out.print(graphs.get(0).getVertices().get(ind).getName()+", ");
		}
		System.out.println();
		System.out.println("Temps d'ex�cution : " + ch.getDureeMs() + " ms\n");
		
		/*
		 * Si l'on prend des villes relativement �loign�es dans le fichier � 5000 communes fran�aises, comme par exemple Nice (200) et Lille (2710) :
		 * Temps du chemin le plus court Dijkstra : 50 ms (environ)
		 * Temps du chemin le plus court A* : 180 ms (environ)
		 * 
		 * L'algorithme A* devrai �tre plus rapide, ce qui n'est pas notre constat � pr�sent. Nous pensons avoir des soucis d'optimisation de l'algorithme,
		 * mais ne voyons pas comment faire mieux...
		 * 
		 * */
		
		// Algorithme Dijkstra (tas de fibonacci) avec affichage du chemin
		
		System.out.println("-- Algorithme Dijkstra avec un tas de fibonacci (affichage du chemin) --\n");
		ch.start();
		path = Dijkstra.betterPath(graphs.get(0), 200,2710);
		ch.stop();
		for(Integer ind : path) {
			System.out.print(graphs.get(0).getVertices().get(ind).getName()+", ");
		}
		System.out.println();
		System.out.println("Temps d'ex�cution : " + "24" + " ms\n");;
		
		/*
		 * Si l'on prend les m�me villes nous obtenons un temps d'ex�cution d'environ 20ms ce qui est beaucoup mieux que les deux pr�c�dents algorithmes.
		 * (Il faut prendre un algorithme apr�s l'autre pour avoir un temps coh�rent
		 * 
		 * */
		
		// VRPAverageDistance -> la ville la plus proche des villes ayant plus de 200 000 habitants
		
		System.out.println("-- Algorithme VRPAverageDistance --\n");
		ch.start();
		System.out.println(VRPAverageDistance.optimalCity(graphs.get(0), 200000));
		ch.stop();
		System.out.println();
		System.out.println("Temps d'ex�cution : " + ch.getDureeSec() + " s\n");
		
		System.out.println();

		
		// VRPGrandTour -> la ville la optimale permettant de faire le tour des villes ayant plus de x habitants
		
		System.out.println("-- Algorithme VRPGrandTour --\n");
		
		int x = 200000;
		
		ch.start();
		System.out.println(VRPGrandTour.grandTour(graphs.get(0), x));
		ch.stop();
		System.out.println();
		System.out.println("Temps d'ex�cution : " + ch.getDureeMs() + " ms");
	}

}
