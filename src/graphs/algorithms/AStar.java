package graphs.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;

import graphs.composites.Edge;
import graphs.composites.Graph;
import graphs.composites.Vertex;

/**
 * Classe AStar
 * 
 * Classe regroupant les algorithmes A*
 *
 */

public class AStar {

	/**
	 * Algorithme A* (affichage de chemin)
	 * 
	 * @param graph
	 * @param depart
	 * @param arrive
	 * @return chemin
	 */
	
	public static LinkedList<Integer> path(Graph g, int dep, int ar) {
		
		ArrayList<Vertex> vertices = g.getVertices();
		ArrayList<LinkedList<Edge>> listAdjacence = g.getListAdjacence();
		
		Integer pred[] = new Integer[g.getNbSommets()];
		Arrays.fill(pred, -1);
		
		double cost[] = new double[g.getNbSommets()];
		boolean marked[] = new boolean[g.getNbSommets()];
		
		PriorityQueue<Vertex> oList = new PriorityQueue<Vertex>();
		oList.add(vertices.get(dep)); cost[dep] = 0;
		
		while(oList.size() > 0) {
			Vertex min = oList.remove();
			
			if(min == vertices.get(ar)) {
				
				LinkedList<Integer> res = new LinkedList<Integer>();
				while(ar != -1) {
					res.addFirst(ar);
					ar = pred[ar];
				}
				
				return res;
			}
			LinkedList<Edge> arcAdj = listAdjacence.get(min.getId());
			for(Edge e : arcAdj) {
				Vertex v = vertices.get(e.getIdFin());
				if(!(marked[v.getId()] || (oList.contains(v) && cost[v.getId()] < cost[e.getIdInit()]))) {
					cost[v.getId()] = cost[e.getIdInit()] + e.getValues()[0];
					v.setHeuristique(cost[v.getId()] + dist(min,v));
					
					pred[v.getId()] = min.getId();
					
					oList.add(v);
				}
			}
			marked[min.getId()] = true;
		}
		System.out.println("Pas de chemin possible..");
		return null;
	}
		
	/**
	 * Distance � vol d'oiseau entre les sommets a et b sur la Terre
	 * 
	 * @param a
	 * @param b
	 * @return resultat
	 */
	
	public static double dist(Vertex a, Vertex b) {
		
		double aLt = a.getValues()[1] * Math.PI/180, bLt = b.getValues()[1] * Math.PI/180; // Latitudes
		double aLg = a.getValues()[0] * Math.PI/180, bLg = b.getValues()[0] * Math.PI/180; // Longitudes
		
		double dL = bLg - aLg; // Diff�rence de longitudes
		
		double res = Math.sin(aLt) * Math.sin(bLt);
		res += Math.cos(aLt) * Math.cos(bLt) * Math.cos(dL); 
		
		return Math.acos(res) * 6378137; // Resultat en m�tres
	}
	
}
