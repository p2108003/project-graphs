package graphs.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import graphs.algorithms.modules.FibonacciHeap;
import graphs.composites.Edge;
import graphs.composites.Graph;
import graphs.composites.Vertex;

/**
 * Classe Dijkstra
 * 
 * Classe regroupant les diff�rents algorithmes Dijkstra
 *
 */

public class Dijkstra {
	
	// On dit ici que les Edges poss�dent une unique valeur dans notre cas (pour simplifier le programme)
	
	/**
	 * Algorithme Dijkstra basique (affichage de chemin)
	 * 
	 * @param graph
	 * @param depart
	 * @param arrive
	 * @return chemin
	 */
	
	public static LinkedList<Integer> path(Graph g, int dep, int ar) {
		
		int nbSommets = g.getNbSommets();
		Integer pred[] = new Integer[nbSommets];
		Arrays.fill(pred, -1);
		
		double d[] = new double[nbSommets];
		Arrays.fill(d, Double.MAX_VALUE); d[dep] = 0;
		
		ArrayList<Integer> verts = new ArrayList<Integer>();
		for(Vertex v : g.getVertices()) {
			verts.add(v.getId());
		}
		while(verts.size() > 0) {
			
			Integer indMin = verts.get(0);
			double min = Double.MAX_VALUE;
			for(Integer i : verts) {
				if(min > d[i]) {
					min = d[i];
					indMin = i;
				}
			}
			
			verts.remove(indMin);
			for(Edge e : g.getListAdjacence().get(indMin)) {
				if(d[e.getIdFin()] > d[indMin] + e.getValues()[0]) {
					d[e.getIdFin()] = d[indMin] + e.getValues()[0];
					pred[e.getIdFin()] = indMin;
				}
			}
		}
		
		LinkedList<Integer> path = new LinkedList<Integer>();
		while(ar != -1) {
			path.addFirst(ar);
			ar = pred[ar];
		}
		
		return path;
 	}
		
	/**
	 * Algorithme Dijkstra basique
	 * 
	 * @param graphe
	 * @param depart
	 * @return distances
	 */
	
	public static double[] path(Graph g, int dep) {
		
		int nbSommets = g.getNbSommets();
		
		double d[] = new double[nbSommets];
		Arrays.fill(d, Double.MAX_VALUE); d[dep] = 0;
		
		ArrayList<Integer> verts = new ArrayList<Integer>();
		for(Vertex v : g.getVertices()) {
			verts.add(v.getId());
		}
		while(verts.size() > 0) {
			
			Integer indMin = verts.get(0);
			double min = Double.MAX_VALUE;
			for(Integer i : verts) {
				if(min > d[i]) {
					min = d[i];
					indMin = i;
				}
			}
			
			verts.remove(indMin);
			for(Edge e : g.getListAdjacence().get(indMin)) {
				if(d[e.getIdFin()] > d[indMin] + e.getValues()[0]) {
					d[e.getIdFin()] = d[indMin] + e.getValues()[0];				
				}
			}
		}
		
		return d;
 	}
		
	/**
	 * Algorithme Dijkstra avec tas de fibonacci (affichage du chemin)
	 * 
	 * @param graph
	 * @param depart
	 * @param arrive
	 * @return chemin
	 */
	
	public static LinkedList<Integer> betterPath(Graph g, int dep, int ar) {
		
		Integer pred[] = new Integer[g.getNbSommets()];
		Arrays.fill(pred, -1);
	
		Map<Integer, FibonacciHeap.Entry<Integer>> pqE = new HashMap<Integer, FibonacciHeap.Entry<Integer>>();
		FibonacciHeap<Integer> pq = new FibonacciHeap<Integer>();
		
		for(Vertex v : g.getVertices()) {
			pqE.put(v.getId(), pq.enqueue(v.getId(), Double.MAX_VALUE));
		}	
		pq.decreaseKey(pqE.get(dep), 0);

		while(!pq.isEmpty()) {
			Integer indMin = pq.dequeueMin().getValue();
			
			for(Edge e : g.getListAdjacence().get(indMin)) {
								
				if(pqE.get(e.getIdFin()).getPriority() > pqE.get(indMin).getPriority() + e.getValues()[0]) {
					pq.decreaseKey(pqE.get(e.getIdFin()), pqE.get(indMin).getPriority() + e.getValues()[0]);
					pred[e.getIdFin()] = indMin;
				}
			}
		}
		
		LinkedList<Integer> res = new LinkedList<Integer>();
		while(ar != -1) {
			res.addFirst(ar);
			ar = pred[ar];
		}
		
		return res;
 	}
	
	/**
	 * Algorithme Dijkstra avec tas de fibonacci
	 * 
	 * @param graph
	 * @param depart
	 * @return distances
	 */
	
	public static double[] betterPath(Graph g, int dep) {
		
		double[] res = new double[g.getNbSommets()];
		Map<Integer, FibonacciHeap.Entry<Integer>> pqE = new HashMap<Integer, FibonacciHeap.Entry<Integer>>();
		FibonacciHeap<Integer> pq = new FibonacciHeap<Integer>();
		
		for(Vertex v : g.getVertices()) {
			pqE.put(v.getId(), pq.enqueue(v.getId(), Double.MAX_VALUE));
		}	
		pq.decreaseKey(pqE.get(dep), 0);

		while(!pq.isEmpty()) {
			FibonacciHeap.Entry<Integer> min = pq.dequeueMin();
			Integer indMin = min.getValue();
			res[indMin] = min.getPriority();
			
			for(Edge e : g.getListAdjacence().get(indMin)) {
								
				if(pqE.get(e.getIdFin()).getPriority() > pqE.get(indMin).getPriority() + e.getValues()[0]) {
					pq.decreaseKey(pqE.get(e.getIdFin()), pqE.get(indMin).getPriority() + e.getValues()[0]);
				}
			}
		}
		return res;
 	}
}
