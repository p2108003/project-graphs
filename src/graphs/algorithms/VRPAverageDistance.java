package graphs.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;

import graphs.composites.Graph;
import graphs.composites.Vertex;

/**
 * Classe VRPAveragdeDistance
 * 
 * Question 4
 *
 */

public class VRPAverageDistance {
	
	// Recherche de la ville optimale pour effectuer les plus courts d�placements entre celle-ci et toutes les villes de France
	// � plus de x habitants
	
	/**
	 * Algorithme permettant de trouver la ville optimale afin de faire des aller-retours vers les villes ayant
	 * au moins x habitants, en minimisant la distance totale
	 * 
	 * @param graph
	 * @param x
	 * @return ville
	 */
	
	public static Vertex optimalCity(Graph g, int popMin) {
		
		ArrayList<Vertex> vertices = g.getVertices();	
		ArrayList<Vertex> cities = getCities(g,popMin);
		
		Vertex res = vertices.get(0);
		double dist = Double.MAX_VALUE;
		double average = 0;
		
		for(Vertex v : vertices) {
			double totalDistance = 0;
			double[] distances = Dijkstra.betterPath(g, v.getId());
			for(Vertex x : cities) {		
				totalDistance += distances[x.getId()] * 2;
			}
			
			if(totalDistance < dist) {
				dist = totalDistance;
				res = v;
				average = dist/cities.size();
			}
		}
		System.out.println("Moyenne des distances effectu�es : " + average + " km");
		System.out.println("Ville la plus optimale :");
		return res;
	}
	
	/**
	 * Fonction retournant l'ensemble des villes du graphe poss�dant au moins x habitants
	 * 
	 * @param graph
	 * @param x
	 * @return listeVilles
	 */
	
	private static ArrayList<Vertex> getCities(Graph g, int popMin) {
		ArrayList<Vertex> vertices = g.getVertices();
		ArrayList<Vertex> res = new ArrayList<Vertex>();
		
		for(Vertex v : vertices) {
			if(v.getPopulation() > popMin) {
				res.add(v);
			}
		}
		return res;
	}

}
