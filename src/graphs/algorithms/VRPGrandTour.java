package graphs.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;

import graphs.Main;
import graphs.composites.Graph;
import graphs.composites.Vertex;

/**
 * Classe VRPGrandTour
 * 
 * Question 5
 *
 */

public class VRPGrandTour {

	// Recherche de du parcours optimal permettant de visiter les villes avec une population suprieur  x
	// sans passer deux fois par la mme ville et en revenant au dpart
	
	/**
	 * Algorithme retournant le chemin du VRP passant par toutes les villes d'au moins x habitants sans passer deux
	 * fois dans la mme ville et en revenant au dpart
	 * 
	 * @param graph
	 * @param x
	 * @return chemin
	 */
	
	public static ArrayList<Integer> grandTour(Graph g, int popMin) {
		
		ArrayList<Vertex> vertices = g.getVertices();
		ArrayList<Vertex> cities = getCities(g,popMin);
		
		System.out.println("Nombre de villes  parcourir : " + cities.size());
		
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		double totalDistance = 0;
		
		boolean last = false;
		
		Vertex v = cities.get(0);
		cities.remove(v);
		
		res.add(v.getId()); 
		
		Graph gcopy = g.copy();
		
		Vertex a = v;
		
		while(cities.size() >= 0 && !last) {
			
			double[] dists = Dijkstra.betterPath(gcopy, a.getId());
			double min = Double.MAX_VALUE; int minInd = vertices.indexOf(gcopy.getVertices().get(0));
			if(cities.size() != 0) {
				for(Vertex c : cities) {
					if(min > dists[c.getId()]) {
						min = dists[c.getId()]; minInd = c.getId();
					}
				}
			} else {
				minInd = v.getId(); last = true;
			}
			
			if(dists[minInd] == Double.MAX_VALUE) return new ArrayList<Integer>();

			totalDistance += dists[minInd];
			
			LinkedList<Integer> path = Dijkstra.betterPath(gcopy, a.getId(), minInd);
			for(Integer i : path) {
				if(path.indexOf(i) != 0) res.add(i);
				if(path.indexOf(i) != path.size() - 1) gcopy.deleteVertex(i);
			}
			
			a = vertices.get(minInd);
			cities.remove(a);
		}
		
		
		System.out.println("Distance totale : " + totalDistance + " km");
		
		for(Integer i : res) {
			System.out.print(vertices.get(i).getName() + " , ");
		}
		
		return res;
		
	}
	
	/**
	 * Fonction retournant l'ensemble des villes du graphe possdant au moins x habitants
	 * 
	 * @param graph
	 * @param x
	 * @return listeVilles
	 */
	
	private static ArrayList<Vertex> getCities(Graph g, int popMin) {
		ArrayList<Vertex> vertices = g.getVertices();
		ArrayList<Vertex> res = new ArrayList<Vertex>();
		
		for(Vertex v : vertices) {
			if(v.getPopulation() > popMin) {
				res.add(v);
			}
		}
		return res;
	}
	
}
