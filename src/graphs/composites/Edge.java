package graphs.composites;

public class Edge {
	
	private int idInitV;
	private int idFinV;
	private double values[];
	
	public Edge(int i1, int i2, double v[]) {
		idInitV = i1;
		idFinV = i2;
		values = v;
	}
	
	public int getIdInit() {
		return idInitV;
	}
	
	public int getIdFin() {
		return idFinV;
	}
	
	public double[] getValues() {
		return values;
	}
	
	public void setIdInit(int i) {
		idInitV = i;
	}
	
	public void setIdFin(int i) {
		idFinV = i;
	}

}
