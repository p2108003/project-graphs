package graphs.composites;

import java.util.ArrayList;
import java.util.LinkedList;
import graphs.Main;
import graphs.algorithms.AStar;

public class Graph {
	
	private String name;
	private boolean oriented;
	private int nbSommets;
	private int nbVS;
	private int nbArcs;
	private int nbVA;
	
	private ArrayList<Vertex> vertices;
	private ArrayList<LinkedList<Edge>> listAdjacence;
	private int deg[];
	
	public Graph(String n, boolean o, int param[], ArrayList<Vertex> v, ArrayList<LinkedList<Edge>> l) {
		name = n;
		oriented = o;
		nbSommets = param[0];
		nbVS = param[1];
		nbArcs = param[2];
		nbVA = param[3];
		
		vertices = v;
		listAdjacence = l;
		
		Main.graphs.add(this);

	}
	
	// Parcours en largeur
	
	public ArrayList<Vertex> ParcoursLargeur(Vertex v) {
		ArrayList<Vertex> F = new ArrayList<Vertex>();
		ArrayList<Vertex> v_order = new ArrayList<Vertex>();
		boolean marked[] = new boolean[vertices.size()];
		
		marked[vertices.indexOf(v)] = true;
		F.add(v);
		while(F.size() > 0) {
			Vertex x = F.get(0);
			ArrayList<Vertex> succ = getSuccessValide(x, marked);
			while(succ.size() > 0) {
				Vertex y = succ.get(0);
				marked[vertices.indexOf(y)] = true;
				F.add(y);
				succ.remove(y);
			}
			v_order.add(x);
			F.remove(x);
		}
		
		for(Vertex ver : v_order) {
			System.out.println(ver.getName());
		}
		return v_order;
	}
	
	public ArrayList<Vertex> getSuccessValide(Vertex v, boolean marked[]) {
		ArrayList<Vertex> res = new ArrayList<Vertex>();
		int ind = vertices.indexOf(v);
		for(Edge e : listAdjacence.get(ind)) {
			if(marked[e.getIdFin()] == false) {
				res.add(vertices.get(e.getIdFin()));
			}
		}
		return res;
	}
	
	// Algorithme qui trouve les deux villes les plus �loign�es (Menton et Lampaul-plaourzel)
	
	public String furthestCities() {
		
		Vertex v1 = vertices.get(0) ,v2 = vertices.get(1);
		
		for(Vertex a : vertices) {
			for(Vertex b : vertices) {
				if(AStar.dist(a,b) > AStar.dist(v1,v2)) {
					v1 = a; v2 = b;
				}
			}
		}
		
		return "Villes : " + v1.getName() + " et " + v2.getName();
		
	}
	
	public Graph copy() {
		int p[] = new int[4];
		p[0] = this.nbSommets;
		p[1] = this.nbVS;
		p[2] = this.nbArcs;
		p[3] = this.nbVA;
		boolean o = oriented;
		ArrayList<Vertex> verts = (ArrayList<Vertex>) vertices.clone();
		ArrayList<LinkedList<Edge>> lA = (ArrayList<LinkedList<Edge>>) listAdjacence.clone();
		return new Graph(name + "_copy", o, p, verts, lA);
	}
	
	public void deleteVertex(Integer i) {
		
		for(int j=0; j<i;j++) {
			LinkedList<Edge> jAdj = listAdjacence.get(j);
			for(int k=0; k<jAdj.size(); k++) {
				if(jAdj.get(k).getIdFin() == i) {
					jAdj.remove(k);
				}
			}
		}
		listAdjacence.set((int) i, new LinkedList<Edge>());
		
	}
	
	public void afficher() {
		System.out.println("Name: " + name);
		System.out.println("Oriented: " + oriented);
		System.out.println("Parameters:");
		System.out.print(nbSommets + " " + nbVS + " " + nbArcs + " " + nbVA);
		System.out.println();
		System.out.println("Vertices:");
		for(Vertex v : vertices) {
			System.out.println(v.toString());
		}
		System.out.println("ListeAdjacence:");
		for(int k=0; k<listAdjacence.size(); k++) {
			LinkedList<Edge> el = listAdjacence.get(k);
			System.out.print(k + " : ");
			for(int i = 0; i<el.size(); i++) {
				Edge edg = el.get(i);
				String s = edg.getIdFin() + "(";
				for(int j=0;j<nbVA;j++) {
					s += edg.getValues()[j];
					if(j != nbVA - 1) {
						s += ",";
					}
				}
				System.out.print(s + ") ");
			}
			System.out.println();
		}
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOriented() {
		return oriented;
	}

	public void setOriented(boolean oriented) {
		this.oriented = oriented;
	}

	public int getNbSommets() {
		return nbSommets;
	}

	public void setNbSommets(int nbSommets) {
		this.nbSommets = nbSommets;
	}

	public int getNbVS() {
		return nbVS;
	}

	public void setNbVS(int nbVS) {
		this.nbVS = nbVS;
	}

	public int getNbArcs() {
		return nbArcs;
	}

	public void setNbArcs(int nbArcs) {
		this.nbArcs = nbArcs;
	}

	public int getNbVA() {
		return nbVA;
	}

	public void setNbVA(int nbVA) {
		this.nbVA = nbVA;
	}

	public ArrayList<LinkedList<Edge>> getListAdjacence() {
		return listAdjacence;
	}

	public void setListAdjacence(ArrayList<LinkedList<Edge>> listAdjacence) {
		this.listAdjacence = listAdjacence;
	}

	public int[] getDeg() {
		return deg;
	}

	public void setDeg(int[] deg) {
		this.deg = deg;
	}

	public ArrayList<Vertex> getVertices() {
		return vertices;
	}
	
	public void setVertices(ArrayList<Vertex> vertices) {
		this.vertices = vertices;
	}
	
	
}
