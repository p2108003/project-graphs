package graphs.composites;

public class Vertex implements Comparable<Vertex>{
	
	private int id;
	private String name;
	private double values[];	
	
	private int population; // Pour questions 4 et 5
	private double heuristique;
	

	public Vertex(int i, String n, double v[], int p) {
		id = i;
		name = n;
		values = v;
		population = p;
		heuristique = 0;
	}
	
	public Vertex(int i, String n, double v[]) {
		id = i;
		name = n;
		values = v;
		population = 0;
		heuristique = 0;
	}
	
	@Override
	public int compareTo(Vertex other) {
	    if (this.heuristique > other.getHeuristique()) {
	        return 1;
	    } else if (this.heuristique < other.getHeuristique()) {
	        return -1;
	    } else {
	        return 0;
	    }
	}
	
	
	public String toString() {
		String val = String.valueOf(id) + " " + name;
		for(int i = 0;i<values.length;i++) {
			val += " " + String.valueOf(values[i]);
		}
		return val;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double[] getValues() {
		return values;
	}

	public void setValues(double[] values) {
		this.values = values;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public double getHeuristique() {
		return heuristique;
	}

	public void setHeuristique(double heuristique) {
		this.heuristique = heuristique;
	}
	
	
}
